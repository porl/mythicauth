<!-- Title suggestion: [Story] Name of story -->

## User Story

As a player/user/DM/developer, I can ... 

## Details

Any related information

## Acceptance Criteria

- [ ] Use case ...
- [ ] Use case ...
- [ ] Use case ...

## Data concerns

If there are any concerns, not them here.  Otherwise remove this section

## Security concerns

If there are any concerns, not them here.  Otherwise remove this section

## Stages

- [ ] Planned accepted
- [ ] Code Complete
- [ ] Contains tests
- [ ] Documentation updated
- [ ] Contains log changes
- [ ] Analytics included
- [ ] Deployed to `edge`
- [ ] Announced

/label ~"Story"
/milestone %"First Playable" 
/weight 2
<!-- A note on weights
0 is trivial. Does not involve and code
1 is a button, documentation corrections, a quick fix
2 is a simple well-known task
3 is for a somewhat simple task
5 is for a complicated story that requires a number of moving parts
8 is for a significant amount of work but should with perhaps some unknowns or external dependencies
Anything higher than 8 should be broken up into smaller more distinct stories.
-->