variables:
    # When using dind service we need to instruct docker, to talk with the
    # daemon started inside of the service. The daemon is available with
    # a network connection instead of the default /var/run/docker.sock socket.
    #
    # The 'docker' hostname is the alias of the service container as described at
    # https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#accessing-the-services
    #
    # Note that if you're using Kubernetes executor, the variable should be set to
    # tcp://localhost:2375 because of how Kubernetes executor connects services
    # to the job container
    DOCKER_HOST: tcp://docker:2375/
    # When using dind, it's wise to use the overlayfs driver for
    # improved performance.
    DOCKER_DRIVER: overlay2
    CONTAINER_IMAGE: $CI_REGISTRY_IMAGE
    CONTAINER_TAG: $CI_PIPELINE_IID-$CI_COMMIT_SHORT_SHA
    DOCKER_ASPNETCORE_IMAGE: mcr.microsoft.com/dotnet/core/sdk:3.1-alpine

stages:
    - build
    - test
    - package
    - containerize
    - deploy

build:
    image: $DOCKER_ASPNETCORE_IMAGE
    stage: build
    script:
        - dotnet restore --packages .nuget
        - dotnet build -c Release --no-restore --packages .nuget
    artifacts:
        name: "auth-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
        paths:
            - src/MythicAuth/bin
            - src/MythicAuth/obj/project.assets.json
        expire_in: 1h
    cache:
        key: nuget
        paths:
            - .nuget/

test-client-js:
    image: node:lts-alpine
    stage: test
    before_script:
        - npm install
    script:
        - npm test

test-server:
    image: $DOCKER_ASPNETCORE_IMAGE
    stage: test
    dependencies:
        - build
    script:
        - dotnet test tests/MythicAuth.Tests/MythicAuth.Tests.csproj
        - dotnet test tests/MythicAuth.Integration.Tests/MythicAuth.Integration.Tests.csproj

package:
    image: $DOCKER_ASPNETCORE_IMAGE
    stage: package
    only:
        - master
    dependencies:
        - test-client-js
        - test-server
    script:
        - dotnet publish -c Release --packages .nuget -o ./package src/MythicAuth/
    artifacts:
        name: "mythicauth-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
        paths:
            - package/
    cache:
        key: nuget
        paths:
            - .nuget/

containerize:
    stage: containerize
    image: docker:stable
    only:
        - master
    services:
        - docker:dind
    dependencies: [ package ]
    script:
        - docker info
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
        - docker build -f Dockerfile.gitlab-ci -t $CONTAINER_IMAGE:$CONTAINER_TAG -t $CONTAINER_IMAGE:latest .
        - docker push $CONTAINER_IMAGE:$CONTAINER_TAG
        - docker push $CONTAINER_IMAGE:latest


deploy-production:
    stage: deploy
    image: dtzar/helm-kubectl
    only:
        - master
    environment:
        name: mythic-table-edge-02
        url: https://auth.edge.mythictable.com
    script:
        - kubectl config set-cluster mythic-table-edge-02 --server=$KUBE_URL
        - sed -i "s/<VERSION>/$CONTAINER_TAG/g" deployment.yaml
        - sed -i "s/<CI_ENVIRONMENT_SLUG>/$CI_ENVIRONMENT_SLUG/g" deployment.yaml
        - sed -i "s/<CI_PROJECT_PATH_SLUG>/$CI_PROJECT_PATH_SLUG/g" deployment.yaml
        - kubectl --token=$KUBE_TOKEN apply -f deployment.yaml
