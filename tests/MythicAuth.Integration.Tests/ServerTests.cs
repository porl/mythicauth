using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;

using MythicAuth;
using Xunit;

namespace MythicAuth.Integration.Tests
{
    public class ServerTests : IClassFixture<WebApplicationFactory<MythicAuth.Startup>>
    {
        private readonly WebApplicationFactory<MythicAuth.Startup> _factory;

        public ServerTests(WebApplicationFactory<MythicAuth.Startup> factory)
        {
            _factory = factory;
        }


        [Theory]
        [InlineData("/")]
        [InlineData("/Account/Login")]
        [InlineData("/Account/Register")]
        [InlineData("/Account/Logout")]
        public async Task UrlCheck(string url)
        {
            var client = _factory.CreateClient();

            var resp = await client.GetAsync(url);

            resp.EnsureSuccessStatusCode();
        }
    }
}
