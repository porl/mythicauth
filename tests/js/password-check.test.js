const passReqs = require("../../src/MythicAuth/wwwroot/js/password-check");
const assert = require("assert");
const nock = require("nock");
const fetch = require("node-fetch");

global.fetch = fetch;

const passwordsTest = {
    "password": {
        "hash": "5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8",
        "breaches": 20,
        "breached": true
    },
    "correcthorsebatterystaple": {
        "hash": "bfd3617727eab0e800e62a776c76381defbc4145",
        "breaches": 0,
        "breached": false
    },
    "password1": {
        "hash": "e38ad214943daad1d64c102faec29de4afe9da3d",
        "breaches": 1,
        "breached": true
    }
};

for(let key of Object.keys(passwordsTest)){
    nock(passReqs.pwnURL).get(`/${passwordsTest[key]["hash"].substr(0, 5)}`)
        .reply(200, (uri, requestBody)=> {

            for (let key of Object.keys(passwordsTest)){
                if(passwordsTest[key]["breached"]){
                    requestBody += `${passwordsTest[key]["hash"].substr(5).toUpperCase()}:${passwordsTest[key]["breaches"]}\n`;
                }
            }

            return requestBody;
        });
}

test("Test Sha-1 hashing", async () => { 
   for (let key of Object.keys(passwordsTest)){
       let genHash = await passReqs.getSha1(key);
       assert(passwordsTest[key]["hash"] === genHash, `Expected:\t${passwordsTest[key]["hash"]}\nGot:\t${genHash}`);
   }
});

test("Test isBreached", async () => {
    for(let key of Object.keys(passwordsTest)){
        let result = await passReqs.isBreached(key);

        assert(result === passwordsTest[key]["breached"], `Expected isBreached(${key}) to be ${passwordsTest[key]["breached"]}, but got ${result}`)
    }
});
