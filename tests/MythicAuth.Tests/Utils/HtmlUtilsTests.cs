using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Identity;

using Xunit;
using MythicAuth;

namespace MythicAuth.Tests {
    public class MockHostEnv : IHostEnvironment
    {
        public string ApplicationName { get; set; }
        public Microsoft.Extensions.FileProviders.IFileProvider ContentRootFileProvider { get; set; }
        public string ContentRootPath { get; set; }
        public string EnvironmentName { get; set; }
    }
    

    public class HtmlUtilsTests
    {
        public Dictionary<string, Regex> LOGO_ENV_URI = new Dictionary<string, Regex>
        {
            {"Development", new Regex("^http(s)?://localhost:5000")},
            {"Staging", new Regex("^https://edge.mythictable.com")}
        };


        [Fact]
        public void TestConfigure()
        {
            MockHostEnv env = new MockHostEnv();

            foreach(KeyValuePair<string, Regex> entry in LOGO_ENV_URI)
            {
                env.EnvironmentName = entry.Key;
                HtmlUtils.Configure(env);

                Assert.True(entry.Value.Matches(HtmlUtils.clientUri).Count == 1, 
                    String.Format("{0} URI: RegEx {1} did not match {2}", 
                        entry.Key, entry.Value.ToString(), HtmlUtils.clientUri
                    )
                );
            }
        }

        public List<string> GenerateExpectedPassReqs(IdentityOptions opts)
        {
            List<string> output = new List<string>
            {
                    String.Format("Passwords need to be {0} characters long", opts.Password.RequiredLength),
                    String.Format("Passwords need {0} unique characters", opts.Password.RequiredUniqueChars)
            };

            if(opts.Password.RequireUppercase){
                output.Add("Passwords need at least 1 capital letter");
            }
            if(opts.Password.RequireLowercase){
                output.Add("Passwords need at least 1 lowercase letter");
            }
            if(opts.Password.RequireDigit){
                output.Add("Passwords need at least 1 number");
            }
            if(opts.Password.RequireNonAlphanumeric){
                output.Add("Passwords need at least 1 special character (such as !, %, $)");
            }

            return output;
        }

        [Fact]
        public void TestPasswordReqs()
        {
            IdentityOptions opts = new IdentityOptions();


            for(int i = 0; i < 4; ++i)
            {
                opts.Password.RequiredLength = 8;
                opts.Password.RequiredUniqueChars = 3;
                opts.Password.RequireLowercase = true;
                opts.Password.RequireUppercase = i < 2;
                opts.Password.RequireDigit = i < 3;
                opts.Password.RequireNonAlphanumeric = i < 4;

                List<string> pass_reqs = GenerateExpectedPassReqs(opts);

                HtmlUtils.SetPasswordReqs(opts.Password);

                foreach(string req in pass_reqs)
                {
                    Assert.True(HtmlUtils.passReqs.Contains(req), 
                        String.Format("Password Requirements: Could not find \"{0}\" in HtmlUtil's list of password requirements", 
                            req
                        )
                    ); 
                }
            }
        }
    }
}