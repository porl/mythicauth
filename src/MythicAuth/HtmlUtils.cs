using System;
using System.Linq;
using System.Collections.Generic;

using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Identity;

namespace MythicAuth
{
    public static class HtmlUtils
    {        
        public static string clientUri { get; set; }
        public static bool pwnCheck { get; set; } = true;
        public static List<string> passReqs { get; } = new List<string>();

        public static void Configure(IHostEnvironment env)
        {
            string targetId = null;
            if(env.IsDevelopment())
            {
                targetId = "MythicTableLocal";
                
                try
                {
                    pwnCheck = bool.Parse(Environment.GetEnvironmentVariable("CheckPwnedPasswords"));
                }
                catch(FormatException)
                {
                    pwnCheck = true;
                }
                catch(ArgumentNullException)
                {
                    pwnCheck = true;
                }
            }
            else
            {
                targetId = "MythicTableVueFrontEnd";
            }
            clientUri = Config.Clients.Where(c => c.ClientId == targetId)
                .FirstOrDefault().ClientUri;
        }

        public static void SetPasswordReqs(PasswordOptions opts)
        {
            passReqs.Clear();

            passReqs.Add(
                string.Format("Passwords need to be {0} characters long", opts.RequiredLength)
            );
            passReqs.Add(
                string.Format("Passwords need {0} unique characters", opts.RequiredUniqueChars)
            );

            if(opts.RequireLowercase)
            {
                passReqs.Add("Passwords need at least 1 lowercase letter");
            }

            if(opts.RequireUppercase)
            {
                passReqs.Add("Passwords need at least 1 capital letter");
            }

            if(opts.RequireDigit)
            {
                passReqs.Add("Passwords need at least 1 number");
            }

            if(opts.RequireNonAlphanumeric)
            {
                passReqs.Add("Passwords need at least 1 special character (such as !, %, $)");
            }
        }
    }
}
