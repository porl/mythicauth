﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MythicAuth.Data.Migrations
{
    public partial class MythicUserRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MythicRole",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MythicRole",
                table: "AspNetUsers");
        }
    }
}