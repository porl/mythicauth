﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;
using System.Security.Claims;

namespace MythicAuth
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> Ids =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Email(),
                new IdentityResources.Profile(),
            };

        public static IEnumerable<ApiResource> Apis =>
            new ApiResource[]
            {
                // http://docs.identityserver.io/en/latest/reference/api_resource.html
                new ApiResource("mythictableserver", "Mythic Table Server", new[] { JwtClaimTypes.Name })
            };

        public static IEnumerable<Client> Clients =>
            new Client[]
            {
                new Client
                {
                    ClientId = "MythicTableVueFrontEnd",
                    ClientName = "Mythic Table",
                    ClientUri = "https://edge.mythictable.com",
                    RequireClientSecret = false,

                    AllowedGrantTypes = GrantTypes.Code,
                    AllowOfflineAccess = true,
                    RequireConsent = true,
                    RequirePkce = true,

                    // where to redirect to after login
                    RedirectUris = { "https://edge.mythictable.com/oidc" },
                    // where to redirect to after logout
                    PostLogoutRedirectUris = { "https://edge.mythictable.com/home" },
                    AllowedCorsOrigins = { "https://edge.mythictable.com" },

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        "mythictableserver",
                        ClaimTypes.Role
                    }
                },
                new Client
                {
                    ClientId = "MythicTableLocal",
                    ClientName = "Mythic Table Local",
                    ClientUri = "http://localhost:5000",
                    RequireClientSecret = false,

                    AllowedGrantTypes = GrantTypes.Code,
                    AllowOfflineAccess = true,
                    RequireConsent = true,
                    RequirePkce = true,

                    RedirectUris = { "http://localhost:5000/oidc" },
                    PostLogoutRedirectUris = { "http://localhost:5000/" },
                    AllowedCorsOrigins = { "http://localhost:5000" },

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        "mythictableserver",
                        ClaimTypes.Role
                    }
                }
            };
    }
}