//This file requires browserify to update/change behavior
const crypto = require("crypto");

const passwordField = "input#password";
const dismissButton = "button#dismiss";
const alertDiv = "div#passAlert";
const submitButton = "button#register";

const pwnURL = "https://api.pwnedpasswords.com/range";
const separator = ':';

if(!process || !process.env.NODE_ENV){
    $(function(){
        $(passwordField).blur(function() {
            isBreached(this.value).then((compromised) => {
                if(compromised && $(alertDiv).length > 0){
                    $(alertDiv).show();
                    $(submitButton).prop('disabled', true);
                }else {
                    $(alertDiv).hide();
                    $(submitButton).prop('disabled', false);
                }
            });
        });

        $(dismissButton).click(function() {
            $(submitButton).prop('disabled', false);
            $(alertDiv).hide();    
        });

        $(alertDiv).ready(function() {
            $(alertDiv).hide();
        });
    });
}

async function getSha1(pass){
    let result = crypto.createHash("sha1").update(pass);
    return result.digest("hex");
}

async function isBreached(password){
    let sha1 = await getSha1(password);
    let resp = await fetch(`${pwnURL}/${sha1.substr(0,5)}`);

    let body = await resp.text();
    body = body.split('\n');

    let breaches = 0;
    body.forEach((line) => {
        let hashBreach = line.split(separator);
        
        if(hashBreach[0] === sha1.substr(5).toUpperCase()){
            breaches = parseInt(hashBreach[1]);
        }
    });
    return breaches > 0;
}

exports.isBreached = isBreached;
exports.getSha1 = getSha1;
exports.pwnURL = pwnURL;
