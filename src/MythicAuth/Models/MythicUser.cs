﻿using Microsoft.AspNetCore.Identity;

namespace MythicAuth.Models
{
    // Add profile data for application users by adding properties to the MythicUser class
    public class MythicUser : IdentityUser
    {
        public string MythicRole { get; set; }
    }
}