﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;
using System.Linq;
using System.Security.Claims;
using System.Collections.Generic;
using IdentityModel;
using MythicAuth.Data;
using MythicAuth.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Microsoft.Extensions.Hosting;
using System.Threading;
using System.Threading.Tasks;

namespace MythicAuth
{
    public class SeedData: IHostedService
    {
        
        private readonly IServiceProvider _serviceProvider;

        public SeedData(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        
        public Task StartAsync(CancellationToken cancellationToken)
        {
            EnsureSeedData();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;

        public void EnsureSeedData()
        {
            using(var scope = _serviceProvider.CreateScope())
            {
                var myDbContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

                var context = scope.ServiceProvider.GetService<ApplicationDbContext>();

                var userMgr = scope.ServiceProvider.GetRequiredService<UserManager<MythicUser>>();

                CreateUser(userMgr, "marc", email: "marc@example.com", fullName: "Marc Faulise");
                CreateUser(userMgr, "mirko", email: "mirko@example.com", fullName: "Mirko Rainer");
                CreateUser(userMgr, "lenndg", email: "lenndg@example.com", fullName: "Lenn");
                CreateUser(userMgr, "kyra", email: "kyra@example.com", fullName: "Kyra");
                CreateUser(userMgr, "sarah", email: "sarah@example.com", fullName: "Sarah");
                CreateUser(userMgr, "jon", email: "jon@example.com", fullName: "Jon");
                CreateUser(userMgr, "victor", email: "victor@example.com", fullName: "Victor");
                CreateUser(userMgr, "keith", email: "keith@example.com", fullName: "Keith");
            }
        }

        public void CreateUser(UserManager<MythicUser> userMgr, string userName, string fullName = "Full Name", string givenName = "givenName", string familyName = "familyName", string email = "marc@mythictable.com")
        {
            var user = userMgr.FindByNameAsync(userName).Result;
            if (user == null)
            {
                user = new MythicUser
                {
                    UserName = userName,
                    Email = email
                };
                var result = userMgr.CreateAsync(user, "12!@qwQW").Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }

                result = userMgr.AddClaimsAsync(user, new Claim[]{
                        new Claim(JwtClaimTypes.Name, fullName),
                        new Claim(JwtClaimTypes.GivenName, givenName),
                        new Claim(JwtClaimTypes.FamilyName, familyName),
                        new Claim(JwtClaimTypes.Email, email),
                        new Claim(JwtClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean),
                    }).Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }
                Log.Debug($"{user.UserName} created");
            }
            else
            {
                Log.Debug($"{user.UserName} already exists");
            }
        }
    }
}